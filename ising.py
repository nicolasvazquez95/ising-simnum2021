"""
ISING.PY
Última modificación: 11/5/21
N. Vazquez

Script para la simulación del modelo de Ising 2D, con implementación de condiciones de borde periódicas/no periódicas en cada uno de los ejes, y tamaño y temperatura variable.

Se implementa la clase Ising que se encarga de la grilla y los flips,
y la función Ising_Experiment que corre la simulación durante un nro. de pasos arbitrario. 
"""

# Módulos
import numpy as np                             
import matplotlib.pyplot as plt  
import random 

class Ising(object):
    def __init__(self,N,bounds=('p','p')):
        """
        Input
            - N : tamaño de 1 lado de la grilla cuadrada. (N entero positivo)
            - bounds :  2-tupla con las condiciones de borde en cada uno de los ejes. 
            Se lee ('x','y'). Toma 'f' para FBC y 'p' para PBC.
        
            * Si elegimos FBC, la primera y última fila/columna respectivamente se llenan con ceros,
            dejando nulo ese término en el cálculo de las propiedades del sistema.
            (Idea robada del tp de Franco Aquistapace! :) ) 
            * Si elegimos PBC, la primera y última/fila columna respectivamente son nodos fantasma,
            y copian el estado de la primer o última fila/columna de spines, para hacer el cálculo.
            * Por defecto, las condiciones en ambos ejes serán periódicas.
        Elementos de Ising:
            - grid : grilla con los spines inicializados en up/down, representados con +/-1 respectivamente.
        """
        # Inicialización de N+2*N+2 spines aleatorios
            # Privado
        assert N>0,"Tamaño de grilla inválido."
        self.__N = N 
        self.__bounds = bounds
            # Grilla(público)
        self.grid = np.zeros((N+2,N+2),dtype=int)
        for i in range(self.__N+2):
            for j in range(self.__N+2):
                self.grid[i,j] = random.choice((-1,1))
        # Implementamos condiciones de borde:
            # Eje x
        if bounds[0] == 'p':
            self.grid[0,:] = self.grid[self.__N,:]
            self.grid[self.__N+1,:] = self.grid[1,:]
        elif bounds[0] == 'f':
            self.grid[0,:] = 0
            self.grid[self.__N+1,:] = 0
        else:
            raise Exception("Condiciones de borde no válidas, eje x")
            #Eje y
        if bounds[1] == 'p':
            self.grid[:,0] = self.grid[:,self.__N]
            self.grid[:,self.__N+1] = self.grid[:,1]
        elif bounds[1] == 'f':
            self.grid[:,0] = 0
            self.grid[:,self.__N+1] = 0
        else:
            raise Exception("Condiciones de borde no válidas, eje y")
            
    # Función para obtener el N de la grilla (lo hago así para que el valor no pueda cambiar):
    def get_N(self):
        return self.__N
    # Función para obtener las condiciones de borde
    def get_bounds(self):
        return self.__bounds
    
    # Función para hacer un restart random del sistema
    def restart(self):
        for i in range(1,self.__N+1):
            for j in range(1,self.__N+1):
                self.grid[i,j] = random.choice((-1,1))
        # Si hay condiciones periódicas, las pedimos
        if self.__bounds[0] == 'p':
            self.grid[0,:] = self.grid[self.__N,:]
            self.grid[self.__N+1,:] = self.grid[1,:]
        if self.__bounds[1] == 'p':
            self.grid[:,0] = self.grid[:,self.__N]
            self.grid[:,self.__N+1] = self.grid[:,1]
    # Plotear la grilla en matplotlib
    def plot(self,plot_bounds=True):
        """
        Output: Plot de la grilla de spines.
        Los bordes (primera y última fila/columna) implementan condiciones de contorno,
        no forman parte del sistema real.
        Se desactivan seteando bounds = False.
        """
        with plt.xkcd():
            plt.figure(figsize=(10,10))
            if plot_bounds==True:
                plt.matshow(self.grid)
            else:
                plt.matshow(self.grid[1:self.__N+1,1:self.__N+1])
                
    # Cálculo de la energía y la magnetización del sistema
    def __energ_ij(self,i,j,flip=False): #Privado
        """
        Output: Energía de un spin de la grilla, en la posición (i,j)
        Si flip==True, nos da el resultado con el spin flipeado
        """
        e = -self.grid[i,j]*(self.grid[i+1,j]+self.grid[i-1,j]+
                             self.grid[i,j+1]+self.grid[i,j-1])  
        if flip==False: return e
        else: return -e
    def energ_tot(self,promedio=False):
        """
        Input
            - promedio: Si lo seteamos en True, devuelve el valor de la energía promedio (M/N^2)
        Output: Energía total del sistema de spines de la grilla (default: False)
        """
        E = 0
        for i in range(1,self.__N+1):
            for j in range(1,self.__N+1):
                E += self.__energ_ij(i,j)
        if promedio==True:
            return (E/2)/(self.get_N()**2)
        else: return E/2
    def magnetizacion(self,promedio=False):
        """
        Input
            - promedio: Si lo seteamos en True, devuelve el valor de la magnetización promedio (M/N^2)
        Output: Magnetización total del sistema de spines de la grilla
        """
        M = 0
        for i in range(1,self.__N+1):
            for j in range(1,self.__N+1):
                if self.grid[i,j] == 1:M+=1
                else: M-=1
        if promedio==True:
            return M/(self.get_N()**2)
        else: return M
        
    # Funciones para la simulación
    def metropolis_step(self,i,j,T):
        """Esta función nos ayuda a hacer los flips del algoritmo de Metrópolis"""
        deltaE = -2*self.__energ_ij(i,j)
        p = np.exp(-deltaE/T)
        w = random.random()
        if w<=p:
            if self.grid[i,j]==-1: self.grid[i,j] = 1
            else: self.grid[i,j] = -1
        # Actualización spines fantasma (si hay PBC)
        if self.__bounds[0] == 'p':
                self.grid[0,:] = self.grid[self.__N,:]
                self.grid[self.__N+1,:] = self.grid[1,:]
        if self.__bounds[1] == 'p':
                self.grid[:,0] = self.grid[:,self.__N]
                self.grid[:,self.__N+1] = self.grid[:,1]
                
# -------------------------------------------------------                
    
def Ising_Experiment(ising,T,Nsteps=5000,data_step=50):
    """
    Input:
        - ising: Objeto de la clase Ising, inicializado
        - T: Temperatura
        - Nsteps: Número máximo de steps (default:5000)
        - data_step: Cada cuántos steps guardamos data de magnetización y energía (default: 5000/100=50)
    Output:
        3-tupla c/ arrays de numpy:
        - Energ_spin :  Vector con energía promedio por spin, cada data_step steps.
        - Mag_spin : Vector con magnetización promedio por spin cada data_step steps.
    """
    Energ_spin = []
    Mag_spin = []
    # Energía/Magnetización inicial:
    Energ_spin.append(ising.energ_tot(promedio=True))
    Mag_spin.append(ising.magnetizacion(promedio=True))
    for t in range(Nsteps):
        for step in range(ising.get_N()*ising.get_N()):
            i = random.randint(1,ising.get_N())
            j = random.randint(1,ising.get_N())
            ising.metropolis_step(i,j,T)
        if t%data_step==0:
            Energ_spin.append(ising.energ_tot(promedio=True))
            Mag_spin.append(ising.magnetizacion(promedio=True))
    return Energ_spin,Mag_spin
